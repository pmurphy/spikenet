#!/usr/bin/env python
"""
Generate train, validation and test dataset
from UnDySPuTeD spectra files.
Uses spike_positions.csv data annotated by Thibault Peccoux.
Takes no input parameters but `root_path` 
can be changed to something more appropriate.
"""
from multiprocessing import Pool

import astropy.units as u
import matplotlib
matplotlib.use('agg')
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path

from astropy.time import Time, TimeDelta
from skimage import exposure
from skimage.morphology import binary_opening, rectangle, square
from sklearn.model_selection import StratifiedKFold, train_test_split

from nenupy.undysputed import Dynspec

def load_data_from_ds(
        file_names,
        rebin_dt=0.02097152*u.s,
        rebin_df=((200e6/1024)/32)*u.Hz,
        bp_correction="standard",
        fmin=10*u.MHz,
        fmax=90*u.MHz,
        time_range = None,
        stoke="I"):
    """
    Helper function to return data from UnDySPuTeD files.
    Should work ok for small time/frequency ranges.
    Caution advised for time ranges > ~2 mins.
    Default `rebin_dt` and `rebin_df` are the minimum common
    time and frequency resolution between all spectra files
    in `spike_dirs`.
    """
    ds = Dynspec(lanefiles=file_names)
    ds.bp_correction = bp_correction
    ds.dispersion_measure = None
    ds.jump_correction = False

    ds.rebin_dt = rebin_dt
    ds.rebin_df = rebin_df
    print("Loading {}".format(file_names))
    ds.freq_range = [fmin, fmax]
    lane0 = ds.lanes[0]
    if fmax > lane0.fmax:
        ds.beam = 1
    # chop off first and last second in case obeservations aren't aligned
    if time_range is None:
        ds.time_range = [ds.tmin + 1*u.s, ds.tmax - 1*u.s]
    else:
        ds.time_range = time_range
    result = ds.get(stokes=stoke)

    return result

def compute_CUSUM(dspec_data, dt):
    """
    Cumulative Sum Slope.
    dspec_data: background subtracted data
    dt: time resultion of dspec_data
    """
    m = []
    tau = int(0.05/dt.sec)
    for t in range(dspec_data.shape[0]):
        m.append(1/tau * np.sum(dspec_data[t+1:t+tau], axis=0))
    m = np.array(m)
    return m


def load_from_spike_index(df, lanefiles, df_ind):
    """
    Find where in the spectra file the spike burst is.
    Load data centred on spike +- 5s and +- ~390kHz

    df: pandas dataframe of spike locations
    lanefiles: UnDySPuTeD spectra filenames
    df_ind: dataframe index
    """
    centre_time0 = Time(df['time_at_maximum_intensity'][df_ind])
    centre_freq0 = df['frequency_at_maximum_intensity'][df_ind]*u.kHz
    fmin = centre_freq0 - 2*195.3125*u.kHz
    fmax = centre_freq0 + 2*195.3125*u.kHz
    result = load_data_from_ds(lanefiles,
                               bp_correction='standard',
                               fmin=fmin,
                               fmax=fmax,
                               time_range=[
                                   centre_time0 - 5*u.s,
                                   centre_time0 + 5*u.s
                               ],
                               stoke ='I'
                              )
    
    resultV = load_data_from_ds(lanefiles,
                               bp_correction='standard',
                               fmin=fmin,
                               fmax=fmax,
                               time_range=[
                                   centre_time0 - 5*u.s,
                                   centre_time0 + 5*u.s
                               ],
                               stoke='V'
                              )
    return result, resultV


def generate_sample(df, df_ind, result, resultV, aug, output_dir, window_size=64, plot=True, blank=False):
    """
    loads data for time and frequency range specified
    at df_ind.
    default crop to 64x64.
    Calculates CUSUM-Slope to segment burst.
    If aug: shift the centre of the 64x64 tile relative to burst.
    Plot result.
    """
    print("generate spike_id_{} augment_{}".format(str(df_ind), str(aug)))
    centre_time0 = Time(df['time_at_maximum_intensity'][df_ind])
    centre_freq0 = df['frequency_at_maximum_intensity'][df_ind]*u.kHz
    if not blank:
        max_dt = df['instantaneous_duration'][df_ind]*u.s
        max_df = df['instantaneous_spectral_extent'][df_ind]*u.kHz
    else:
        # based on medians from annotated data
        max_dt = 0.5*u.s
        max_df = 104*u.kHz

    if aug != 0:
        # apply random shift in time and frequency
        rng = np.random.default_rng()
        time_shift_direction = 1 if rng.random() > 0.5 else -1
        freq_shift_direction = 1 if rng.random() > 0.5 else -1
        time_shift = max_dt*rng.random()
        freq_shift = 195*u.kHz*rng.random()
    else:
        time_shift = 0
        freq_shift = 0
        time_shift_direction = 1
        freq_shift_direction = 1 

    centre_time = centre_time0 + time_shift_direction*time_shift
    centre_freq = centre_freq0 + freq_shift_direction*freq_shift
    centre_f_pixel = np.argmin(np.abs(result.freq-centre_freq))
    centre_t_pixel = np.argmin(np.abs(result.time-centre_time))

    freq_response = np.percentile(result.amp, 5, axis=0)
    data_normalised = np.zeros((*result.amp.shape, 2))
    data_normalised[:,:,0] = result.amp-freq_response
    data_normalised[:,:,1] = resultV.amp/result.amp
    

    # Only perform CUSUM-slope on central region
    # This mostly avoids complications due to RFI
    centre_f0_pixel = np.argmin(np.abs(result.freq-centre_freq0))
    centre_t0_pixel = np.argmin(np.abs(result.time-centre_time0))    
    detection_arr = np.zeros((data_normalised.shape[0], data_normalised.shape[1]))
    detection_arr = detection_arr.astype('bool')
    cusum_region =\
                data_normalised[centre_t0_pixel-window_size//2:
                                centre_t0_pixel+window_size//2,
                                centre_f0_pixel-window_size//2:
                                centre_f0_pixel+window_size//2,:]
    if not blank:
        # CUSUM-slope detection
        dt = result.time[1] - result.time[0]
        tau = int(0.05/dt.sec)
        cusum = compute_CUSUM(cusum_region[:,:,0], dt)
        percentile_bg = np.percentile(cusum, 5, axis=0) + np.std(cusum)
        detection_arr[centre_t0_pixel-window_size//2:
                    centre_t0_pixel+window_size//2,
                    centre_f0_pixel-window_size//2:
                    centre_f0_pixel+window_size//2] = cusum > percentile_bg
        
        t_range = np.logical_and((result.time > (centre_time0  - max_dt/1.5)),
                                (result.time < (centre_time0  + max_dt/1.5)))
        f_range = np.logical_and((result.freq > centre_freq0 - max_df/1.5),
                                (result.freq < centre_freq0 + max_df/1.5))
        
        spike_location_box = np.zeros_like(detection_arr)
        spike_location_box[np.outer(t_range, f_range)] = 1
        detection_arr = detection_arr & spike_location_box # & detection_arrV
        detection_arr = binary_opening(detection_arr, square(2))

        # shift detection_arr to account for CUSUM-slope offset
        empty_arr = np.zeros_like(detection_arr)
        empty_arr[tau//2:,:] = detection_arr[:-tau//2,:]
        detection_arr = empty_arr

    # 64 x 64 crop
    data_normalised = data_normalised[centre_t_pixel-window_size//2:centre_t_pixel+window_size//2,
                                      centre_f_pixel-window_size//2:centre_f_pixel+window_size//2,:]
    detection_arr = detection_arr[centre_t_pixel-window_size//2:centre_t_pixel+window_size//2,
                                  centre_f_pixel-window_size//2:centre_f_pixel+window_size//2]

    data_clip = np.clip(data_normalised,
                        np.percentile(data_normalised, 0.1, axis=(0,1)),
                        np.percentile(data_normalised, 99.9, axis=(0,1)))
    data_normalisedI = exposure.rescale_intensity(data_clip[:,:,0],
                                           out_range=(0,1))    
    data_normalisedV = exposure.rescale_intensity(data_clip[:,:,1],
                                           out_range=(0,1))    
    data_normalised[:,:,0] = data_normalisedI
    data_normalised[:,:,1] = data_normalisedV

    data_dict = {"data" : data_normalised, 
                 "time" : result.time[centre_t_pixel-window_size//2:centre_t_pixel+window_size//2].isot,
                 "frequency" : result.freq[centre_f_pixel-window_size//2:centre_f_pixel+window_size//2].to(u.MHz).value}
    # plotting
    if plot:
        plt.figure()
        plt.imshow(data_normalised[:,:,0].T, aspect='auto', origin='lower', 
                extent=[result.time[centre_t_pixel-window_size//2].plot_date,
                        result.time[centre_t_pixel+window_size//2].plot_date, 
                        result.freq[centre_f_pixel-window_size//2].to(u.MHz).value, 
                        result.freq[centre_f_pixel+window_size//2].to(u.MHz).value],
                vmin=np.percentile(data_normalised[:,:,0], .1),
                vmax=np.percentile(data_normalised[:,:,0], 99.9))
        # plt.colorbar(orientation='horizontal')
        plt.imshow(1*detection_arr.T, aspect='auto', origin='lower',
                extent=[result.time[centre_t_pixel-window_size//2].plot_date,
                        result.time[centre_t_pixel+window_size//2].plot_date, 
                        result.freq[centre_f_pixel-window_size//2].to(u.MHz).value, 
                        result.freq[centre_f_pixel+window_size//2].to(u.MHz).value],
                cmap = 'magma', alpha=0.25)

        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%S.%f'))
        plt.xlabel("Seconds after {}:00".format(result.time[0].isot[:16]))
        plt.ylabel("Frequency (MHz)")
        plt.savefig("/data/mpearse/spike_dataset/pngs/spike_id_{}_augment_{}".format(str(df_ind).zfill(3),str(aug).zfill(3)))
        plt.close()
    np.save(output_dir/"data_spike_id_{}_augment_{}".format(str(df_ind).zfill(3),
                                                                    str(aug).zfill(3)),
                     data_dict)
    np.save(output_dir/"mask_spike_id_{}_augment_{}".format(str(df_ind).zfill(3),
                                                                    str(aug).zfill(3)),
                     detection_arr)
    return data_dict, detection_arr


if __name__ == "__main__":
    root_path = Path("./")
    df = pd.read_csv(root_path/"spike_positions.csv", index_col=0)
    # fix assumed error at index 123
    df.loc[123,'instantaneous_spectral_extent'] = 30
    
    spike_dirs = [
        Path("/databf/nenufar-tf/ES11/2022/02/20220202_110000_20220202_133000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/ES11/2022/05/20220529_091000_20220529_130000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/05/20230502_091000_20230502_155000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/06/20230601_091000_20230601_135000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/07/20230710_101000_20230710_145000_SUN_TRACKING/"),
        ]

    pandas_dates = pd.to_datetime(df['time_at_maximum_intensity'])
    stratify_dates = np.array(pandas_dates.apply(lambda pd: pd.date()))
    train_split, test_split = train_test_split(df, test_size=0.2, stratify=stratify_dates, random_state=42)
    pandas_dates = pd.to_datetime(train_split['time_at_maximum_intensity'])
    stratify_dates = np.array(pandas_dates.apply(lambda pd: pd.date()))
    # stratify datasets so that there's an equal amount from each observation date
    train_split, val_split = train_test_split(train_split, test_size=0.2, stratify=stratify_dates, random_state=42)
    aug_len = 100 # somewhat arbitrary

    for name, split in zip(["train", "val", "test"], [train_split, val_split, test_split]):
        for df_ind in split.index:
            # keep track of which spectra file to open
            file_ind = 0
            while not split['time_at_maximum_intensity'][df_ind][:10].replace('-','') \
                in spike_dirs[file_ind].as_posix():
                file_ind += 1
            lanefiles = list(spike_dirs[file_ind].glob("*spectra"))
            result, resultV = load_from_spike_index(df, lanefiles, df_ind)
            output_dir = Path(rooth_path"/spike_dataset/{}".format(name))
            
            if name != "test":
                def parallel_aug(aug):
                    return generate_sample(df, df_ind, result, resultV, aug, output_dir, plot=True) 
                
                with Pool() as pool:
                    pool.map(parallel_aug, range(0,aug_len))
             
            else:
                aug = 0
                result, resultV = load_from_spike_index(df, lanefiles, df_ind)
                data_normalised, detection_arr = generate_sample(df, df_ind, result, resultV, aug, output_dir, plot=True)
                