# SpikeNet
Scripts used to generate, train and run SpikeNet model (https://arxiv.org/abs/2403.08546)

`spikes_trainingset_generate.py` creates the data set from a list of NenuFAR spectra files and csv of spike locations.

`spikes_blank_trainingset_generate.py` generates a dataset that does not contain any spike bursts.

`spike_dataset_train.py` trains the model and outputs plots of loss vs epoch etc.

`spike_detect.py` takes a trained model, spectra files, time and frequency range as input and outputs detected spike to a TFCat json file.
Example usage:
```
python spike_detect.py /databf/nenufar-tf/LT11/2024/01/20240120_120000_20240120_125200_SUN_TRACKING/SUN_TRACKING_20240120_120037_0.spectra /databf/nenufar-tf/LT11/2024/01/20240120_120000_20240120_125200_SUN_TRACKING/SUN_TRACKING_20240120_120037_1.spectra --model spike_dataset/model_batch_size_32_regularisation_EarlyStopping_hidden_layers_5_neurons_per_layer_30_n_params_5_2024-01-31T15:00:12 --frange 10 90
```