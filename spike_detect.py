#!/usr/bin/env python
"""
Run SpikeNet on input spectra files
in specified time and frequency range

Outputs TFCat json files of located polygons.

Example usage:
python spike_detect.py \
 /databf/nenufar-tf/LT11/2024/01/20240120_120000_20240120_125200_SUN_TRACKING/SUN_TRACKING_20240120_120037_0.spectra \
 /databf/nenufar-tf/LT11/2024/01/20240120_120000_20240120_125200_SUN_TRACKING/SUN_TRACKING_20240120_120037_1.spectra \
 --model spike_dataset/model_batch_size_32_regularisation_EarlyStopping_hidden_layers_5_neurons_per_layer_30_n_params_5_2024-01-31T15:00:12 \
 --frange 10 90
"""
import argparse
from itertools import product
import os
import pdb
import time
os.environ["SM_FRAMEWORK"] = "tf.keras"

import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf

from pathlib import Path

from astropy.time import Time
from matplotlib import dates
from matplotlib.patches import Rectangle
from shapely.geometry import LinearRing
from skimage import exposure
from tfcat import CRS, dump, Feature, FeatureCollection, Polygon
from tfcat.validate import validate

from nenupy.beamlet import SData
from nenupy.undysputed import Dynspec

from spike_dataset_train import configure_for_performance, rms_for_each

def load_data_from_ds(
        file_names,
        rebin_dt=0.02097152*u.s,
        rebin_df=((200e6/1024)/32)*u.Hz,
        bp_correction="standard",
        fmin=10*u.MHz,
        fmax=90*u.MHz,
        time_range = None,
        stoke="I"):
    """
    Helper function to return data from UnDySPuTeD files.
    Should work ok for small time/frequency ranges.
    
    Assume no errors in data recording and blindly
    concatenate both spectra files together.
    """
    ds = Dynspec(lanefiles=file_names)
    ds.bp_correction = bp_correction
    ds.dispersion_measure = None
    ds.jump_correction = False

    ds.rebin_dt = rebin_dt
    ds.rebin_df = rebin_df
    print("Loading {}".format(file_names))
    #chop off first and last second in case obeservations aren't aligned
    if time_range is None:
        ds.time_range = [ds.tmin + 1*u.s, ds.tmax - 1*u.s]
    else:
        ds.time_range = time_range

    # hacky way to join spectra files. Assumes no errors :/
    if ds.tmin.datetime.year >= 2023:
        ds.freq_range = [ds.lanes[0].fmin, ds.lanes[0].fmax]
        stokes0 = ds.get(stokes=stoke)
        ds.freq_range = [ds.lanes[1].fmin, ds.lanes[1].fmax]
        stokes1 = ds.get(stokes=stoke)
        stokes_conc = np.concatenate((stokes0.data, stokes1.data), axis=1)
        freq_conc = np.concatenate((stokes0.freq, stokes1.freq))
        result = SData(data=stokes_conc, time=stokes0.time, freq=freq_conc,polar=[stoke])
    else:
        ds.freq_range = [fmin, fmax]
        result = ds.get(stokes=stoke)

    return result

def drift_rate_line(time_array, drift_rate, f0, t0, index=True):
    #y-y0 = m(x-x0)
    if index:
        drift_rate =  drift_rate//(pix_df/pix_dt)
    if drift_rate != 0:
        
        freq_array = (drift_rate*(time_array - t0)) + f0
    else:
        freq_array = f0*np.ones(len(time_array))
    return freq_array.astype(int)

def contours_to_polygons(predicted_mask, time_extent, freq_extent, level=0.8, plot=True):
    """
    Use matplotlib to generate contours from predicted_mask
    converts contours to polygons
    """
    if plot:
        plt.figure()
        plt.imshow(predicted_mask.T,aspect="auto", origin="lower", extent=[
                    time_extent[0], time_extent[-1],
                    freq_extent[0], freq_extent[-1]
                ], cmap="magma")
    conts = plt.contour(time_extent, freq_extent, predicted_mask.T, levels=[level], colors='white')
    cont_path = conts.get_paths()[0]
    polys = cont_path.to_polygons()
    return polys

def slice_stoke(stoke, window_len=2048):
    """
    Convert nenupy spectra output to mulitple smaller SData objects
    window_len = 2048 is approx 43 seconds
    """
    slices = np.lib.stride_tricks.sliding_window_view(stoke.amp, window_len, axis=0)
    slice_times = np.lib.stride_tricks.sliding_window_view(stoke.time, window_len)
    
    slices = slices[::window_len]
    slice_times = slice_times[::window_len]
    sdata_list = []
    for slice, slice_time in zip(slices, slice_times):
        sdata_slice = SData(data=slice.T[:,:,np.newaxis], time=Time(slice_time), freq=stoke.freq,polar=[stoke.polar[0]])
        sdata_list.append(sdata_slice)
    return sdata_list

def output_predictions(result, resultV, obs_id, ratio=2):
    """
    Does all the hard work. Could probably be parallelised.
    Main bottleneck is the predict step, scales ~linearly with more input data.
    Solution is to run iteratively on ~3 minute second chunks
    Inputs:
        result: StokesI data from spectra file
        resultV:  StokesV data from spectra file
        obs_id: id of observation
        ratio: int, a proxy for how much to shift the data
            to account for spike burst overlapping the boundaries of a tile
            ratio = 1 --> no shift
            ratio = 2 --> shift by half the tile length
    """
    real_data_gen = Real_Dataset_Generator(result, resultV, ratio=ratio)
    real_ds = tf.data.Dataset.from_generator(real_data_gen,
                                            output_signature=(
                                                tf.TensorSpec(shape=(64,64,4),
                                                                dtype=tf.float64)))
    
    real_ds = real_ds.apply(tf.data.experimental.assert_cardinality(real_data_gen.__len__()))
    real_ds = configure_for_performance(real_ds, batch_size=64, shuffle=False)

    predict_start = time.time()
    real_predict, \
    real_predict_params, \
    real_predict_objectness = regressor_model.predict(real_ds)
    predict_time = time.time() - predict_start

    """
    What follows is ugly but it works, sorry.
    Loop through all predictions and stitch the full 
    dynamic spectrum back together.
    define empty arrays to collect: 
        input stokesI to be stiched together
        predicted mask
        predicted parameters
        predicted objectness
        spike bounding boxes
        spike drift rate
        individual input windows
    """
    parse_start = time.time()
    window_size = 64
    batch_stitch_windows = np.zeros((ratio,
                                     real_data_gen.t_crop,
                                     real_data_gen.f_crop))
    predict_stitch_windows = np.zeros((ratio,
                                       real_data_gen.t_crop,
                                       real_data_gen.f_crop))
    predict_stitch_params = np.zeros((ratio,
                                      (real_data_gen.f_crop//window_size )*(real_data_gen.t_crop//window_size),
                                      5))
    predict_stitch_objectness = np.zeros((ratio,
                                          (real_data_gen.f_crop//window_size )*(real_data_gen.t_crop//window_size)))
    bounding_boxes = np.zeros((ratio,
                               (real_data_gen.f_crop//window_size )*(real_data_gen.t_crop//window_size),
                               4),
                               dtype=object)
    drift_lines = np.zeros((ratio,
                            (real_data_gen.f_crop//window_size )*(real_data_gen.t_crop//window_size),
                            4),
                            dtype=object)
    window_indices = np.zeros((ratio,
                               (real_data_gen.f_crop//window_size )*(real_data_gen.t_crop//window_size),
                               window_size,
                               window_size,
                               4))
    predict_indices = np.zeros((ratio,
                                (real_data_gen.f_crop//window_size )* (real_data_gen.t_crop//window_size), 
                                window_size, 
                                window_size))

    cropped_freq = result.freq.to(u.MHz).value[:real_data_gen.f_crop]
    cropped_time = result.time.plot_date[:real_data_gen.t_crop]

    b = 0 # count window index
    s_counter = 0 # count time shifts
    for batch_in in real_ds.as_numpy_iterator():
        for window in batch_in:
            # find where to place `window` in `batch_stitch_windows`
            freq_overlap = (cropped_freq >= window[0,:,2][0]) & (cropped_freq <= window[0,:,2][-1])
            time_overlap = (cropped_time >= window[:,0,3][0]) & (cropped_time <= window[:,0,3][-1])
            t_loc = np.where(time_overlap)[0][0]
            f_loc = np.where(freq_overlap)[0][0]
            shift = (t_loc%64) // (64//ratio)
            
            batch_stitch_windows[shift, t_loc:t_loc+64, f_loc:f_loc+64] = window[:,:,0]
            predict_stitch_windows[shift, t_loc:t_loc+64, f_loc:f_loc+64] = real_predict[b][:,:,0]
            if real_predict_objectness[b][0] > 0.9:
                # save time by only looking at predictions with > 90% objectness
                freq_pixel = int(f_loc+(64*real_predict_params[b,3]))
                time_pixel = int((t_loc+(64* real_predict_params[b,-1])))
                drift_rate = real_predict_params[b,2]
                width_s = real_predict_params[b,0]
                width = int(width_s/pix_dt)
                
                height_MHz = real_predict_params[b,1]
                height = int(height_MHz/pix_df)
                
                dr = drift_rate_line(np.array([time_pixel - width//2,
                                               time_pixel + width//2]),
                                     real_predict_params[b,2],
                                     freq_pixel,
                                     time_pixel)
                # bounding box defined by: x0,y0 = bottom-left, x1,y1 = top-right
                bounding_box_coords = (result.time[time_pixel - width//2].datetime,
                                    result.freq[freq_pixel - height//2].to(u.MHz).value,
                                    result.time[time_pixel + width//2].datetime,
                                    result.freq[freq_pixel + height//2].to(u.MHz).value)
                
                if shift == 1:
                    array_loc = b - s_counter
                else:
                    array_loc = s_counter
                bounding_boxes[shift, array_loc] = bounding_box_coords
                drift_lines[shift, array_loc] = [result.time[time_pixel - width//2].datetime, 
                                        result.freq[dr[0]].to(u.MHz).value, 
                                        result.time[time_pixel + width//2].datetime, 
                                        result.freq[dr[1]].to(u.MHz).value]

                window_indices[shift, array_loc] = window
                predict_indices[shift, array_loc] = real_predict[b][:,:,0]
                predict_stitch_params[shift, array_loc] = [freq_pixel, time_pixel,height_MHz, width_s, real_predict_params[b,2]]
                predict_stitch_objectness[shift, array_loc] = real_predict_objectness[b][0]
            
            if shift==0:
                s_counter +=1
            b+=1
            
    # filter only best objectness
    best_objectness_loc =  np.argmax(predict_stitch_objectness, axis=0)
    best_objectness_loc_array = np.arange(len(best_objectness_loc))

    best_objectness = predict_stitch_objectness[best_objectness_loc, best_objectness_loc_array]
    above_objectness_threshold = best_objectness != 0
    best_objectness = best_objectness[above_objectness_threshold]

    best_positions = predict_stitch_params[best_objectness_loc,best_objectness_loc_array]
    best_positions = best_positions[above_objectness_threshold]

    best_boxes = bounding_boxes[best_objectness_loc, best_objectness_loc_array]
    best_boxes = best_boxes[above_objectness_threshold]

    best_lines = drift_lines[best_objectness_loc,best_objectness_loc_array]
    best_lines = best_lines[above_objectness_threshold]

    best_params = predict_stitch_params[best_objectness_loc,best_objectness_loc_array].astype(object)
    best_params = best_params[above_objectness_threshold]

    best_windows = window_indices[best_objectness_loc,best_objectness_loc_array]
    best_windows = best_windows[above_objectness_threshold]

    best_predicts = predict_indices[best_objectness_loc,best_objectness_loc_array]
    best_predicts = best_predicts[above_objectness_threshold]

    window_start_freqs = best_windows[:,0,:,2][:,0]
    window_end_freqs = best_windows[:,0,:,2][:,-1]
    window_start_times = Time(best_windows[:,:,0,3], format="plot_date")[:,0]
    window_end_times = Time(best_windows[:,:,0,3], format="plot_date")[:,-1]
    best_params[:,0] = result.freq[best_params[:,0].astype(int)].to(u.MHz)
    best_params[:,1] = Time(result.time[best_params[:,1].astype(int)], format="isot")

    # parse_time = time.time() - parse_start
    # poly_start = time.time()
    
    # output TFCat json
    features = []
    for i, (input_image, predicted_mask) in enumerate(zip(best_windows, best_predicts)):
        freq_extent = input_image[0,:,2]
        time_extent = Time(input_image[:,0,3], format="plot_date").unix
        polys = contours_to_polygons(predicted_mask, time_extent, freq_extent, level=0.5, plot=False)
        if len(polys) >= 1:
            # TODO: correctly handle multipolygons
            max_poly = polys[np.argmax([len(poly) for poly in polys])]
        else:
            print(f"No polygons found for index {i}")
            continue

        if not LinearRing(max_poly).is_ccw:
            max_poly = max_poly[::-1]
        poly_tuples = list(map(tuple,max_poly))
        feature = Feature(
            id=i,
            geometry=Polygon([poly_tuples]),
            properties={
                "feature_type": "solar radio spike",
                
            }
        )
        features.append(feature)
    if len(features) == 0:
        print("No features found, skipping")
        return
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords" :{
                "type": "frequency",
                "unit": "MHz",
            },
        "ref_position_id": "topocenter"
        }
    })

    fields = {
        "feature_type": {
            "info": "Feature Type",
            "datatype": "str",
            "ucd": "meta.id"
        }
    }

    properties = {
            "instrument_host_name": "NenuFAR",
            "instrument_name": "UnDySPuTeD ",
            "target_name": "Sun",
            "target_class": "star",
            "feature_name":"Solar Radio Spike",
            "obs_id":obs_id,
            "model_version":model_path.as_posix(),
            "title": "Solar Radio Spike Bursts located with SpikeNet",
            "authors": [{
                "GivenName": "Pearse C.",
                "FamilyName": "Murphy",
                "ORCID": "https://orcid.org/0000-0002-8606-2645",
                "Affiliation": "https://ror.org/02eptjh02"
            }, {
                "GivenName": "Carine",
                "FamilyName": "Briand",
                "ORCID": "https://orcid.org/0000-0003-0271-9839",
                "Affiliation": "https://ror.org/02eptjh02"
            }, {
                "GivenName": "Stéphane",
                "FamilyName": "Aicardi",
                "ORCID": "https://orcid.org/0000-0003-4475-0930",
                "Affiliation": "https://ror.org/029nkcm90"
            }, {
                "GivenName": "Baptiste",
                "FamilyName": "Cecconi",
                "ORCID": "https://orcid.org/0000-0001-7915-5571",
                "Affiliation": "https://ror.org/02eptjh02"
            }],
            "doi": "https://doi.org/10.25935/m5cq-f460",
            "version": "0.1",
        }

    SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/schema#"
    collection = FeatureCollection(
        schema=SCHEMA_URI,
        features=features,
        properties=properties,
        fields=fields,
        crs=crs,
    )
    # TODO round start time to minute
    validate(collection)
    filename = f"/data/mpearse/spike_dataset/catalogue_spikenet_predictions_{result.time[0].isot[:23]}_{result.time[-1].isot[:23]}.json"
    print(f"Writing {filename} total {len(features)} features")
    with open(filename, "w") as f:
        dump(collection, f)
    
    return 

class Real_Dataset_Generator(tf.keras.utils.Sequence):
    def __init__(self, result, resultV, ratio=2, window_len=64):
        self.result = result
        self.resultV = resultV
        self.ratio = ratio
        self.window_len = window_len
        self.t_crop, self.f_crop = list(self.window_len*(ax//self.window_len) for ax in self.result.amp.shape)
    def getdata(self, stokeI_amp, stokeV_amp, freq_arr, time_arr):
        # print("calculating frequency response")
        freq_response = np.percentile(stokeI_amp, 5, axis=0)
        # print("normalising")
        data_normalised = np.zeros((*stokeI_amp.shape, 2))
        data_normalised[:,:,0] = stokeI_amp-freq_response
        data_normalised[:,:,1] = stokeV_amp/stokeI_amp
        data_clip = np.clip(data_normalised,
                                np.percentile(data_normalised, 1, axis=(0,1)),
                                np.percentile(data_normalised, 99, axis=(0,1)))
        data_normalisedI = exposure.rescale_intensity(data_normalised[:,:,0],
                                               out_range=(0,1))    
        data_normalisedV = exposure.rescale_intensity(data_normalised[:,:,1],
                                               out_range=(0,1))    
        data_normalised[:,:,0] = data_normalisedI
        data_normalised[:,:,1] = data_normalisedV
        # print("concatenating frequency and time")
        freq_tile = np.tile(freq_arr,
                    (data_normalised.shape[0], 1))[:,:,np.newaxis]
        time_tile = np.tile(time_arr,
                    (data_normalised.shape[1],1)).T[:,:,np.newaxis]

        data_normalised = np.concatenate((data_normalised, freq_tile), axis=-1)
        data_normalised = np.concatenate((data_normalised, time_tile), axis=-1)
        
        return data_normalised
        
    def __getitem__(self, stokeI_amp, stokeV_amp, freq_arr, time_arr):
        X = self.getdata(stokeI_amp, stokeV_amp, freq_arr, time_arr)
        return X 
    
    def __call__(self):
        
        stokeI_crop = self.result.amp[:self.t_crop,:self.f_crop]
        stokeV_crop = self.resultV.amp[:self.t_crop,:self.f_crop]
        stokeI_windowed = np.lib.stride_tricks.sliding_window_view(stokeI_crop,
                                                                   window_shape=
                                                                   (self.window_len,
                                                                    self.window_len))[::self.window_len//self.ratio,
                                                                                      ::self.window_len]
        stokeV_windowed = np.lib.stride_tricks.sliding_window_view(stokeV_crop,
                                                                   window_shape=
                                                                   (self.window_len,
                                                                    self.window_len))[::self.window_len//self.ratio,
                                                                                      ::self.window_len]
        freq_windowed = np.lib.stride_tricks.sliding_window_view(self.result.freq[:self.f_crop].to(u.MHz).value,
                                                                 window_shape=(self.window_len))[::self.window_len]
        time_windowed = np.lib.stride_tricks.sliding_window_view(Time(self.result.time[:self.t_crop]).plot_date,
                                                                 window_shape=(self.window_len))[::self.window_len//self.ratio]
        
        for time_arr, stokeI_t, stokeV_t in zip(time_windowed, stokeI_windowed, stokeV_windowed):
            for freq_arr, stokeI_amp, stokeV_amp in zip(freq_windowed, stokeI_t, stokeV_t):
                # print(time_arr[0], freq_arr[0])
                yield self.__getitem__(stokeI_amp, stokeV_amp, freq_arr, time_arr)
    
    def __len__(self):
        # t_crop, f_crop = list(self.window_len*(ax//self.window_len) for ax in self.result.amp.shape)
        return ((self.t_crop//(self.window_len//self.ratio))-(self.ratio-1)) * (self.f_crop//self.window_len)

parser = argparse.ArgumentParser(
    description="Run ML model to detect spikes in given spectra"
)
parser.add_argument('spectra_files',
                    nargs='+',
                    help='Name of input UnDySPuTeD .spectra files')
parser.add_argument('--model',
                    help='ml model file to use')
# parser.add_argument('-s', '--spectra_files', nargs='+', help='Name of NenuFAR dynamic spectra files.')
parser.add_argument('--frange', dest='frange', nargs=2, help='Start frequency, stop frequency in MHz', type=float )
parser.add_argument('--trange', dest='trange', nargs='+',
                        help='time range to predict. Recommended less than 5 minutes \
                        2 arguments START and END in format YYYY-MM-DDTHH:MM:SS\
                        if only START given then assume END is 1 minute later.',
                        metavar=('START', 'END'))
args = parser.parse_args()
spectra_files = args.spectra_files
trange = args.trange
frange = args.frange
model_path = Path(args.model)

if __name__ == "__main__":
    run_start = time.time()
    df = pd.read_csv("/data/mpearse/spike_positions.csv", index_col=0)
    burst_param_keys = df.loc[0].keys()
    burst_param_keys = burst_param_keys.reindex(
                                            [*burst_param_keys.drop(
                                                ['instantaneous_duration',
                                                'instantaneous_spectral_extent',
                                                'time_at_maximum_intensity',
                                                    'minimum_frequency',
                                                    'maximum_frequency']).to_list(), 
                                            'time_at_maximum_intensity'])[0]
    
    custom_objects =  {"RMS_{}".format(param):
                       rms_for_each(i, "RMS_{}".format(param)) for i, param in enumerate(burst_param_keys)}
    regressor_model = tf.keras.models.load_model(model_path, custom_objects=custom_objects)
    fmin = frange[0]*u.MHz
    fmax = frange[1]*u.MHz
    if (trange is not None )and (len(trange) == 1):
            tstart = Time(trange[0])
            tend = tstart + 1 * u.min
            trange = [tstart.isot, tend.isot]

    result = load_data_from_ds(spectra_files,
                            bp_correction='standard',
                            fmin=fmin,
                            fmax=fmax,
                            time_range=trange)
    resultV = load_data_from_ds(spectra_files,
                            bp_correction='standard',
                            fmin=fmin,
                            fmax=fmax,
                            stoke="V",
                            time_range=trange)
    
    print()
    load_time = time.time() - run_start
    obs_id = Path(spectra_files[0]).parts[-2]
    pix_df = result.freq[1] - result.freq[0]
    pix_dt = result.time[1] - result.time[0]
    pix_df = pix_df.to(u.MHz).value
    pix_dt = pix_dt.sec
    ratio = 2 # shift data to account for spikes overlapping boundary edges.
    tlen = result.time[-1] - result.time[0]
    if tlen.sec > 60:
        print("Time range longer than one minute, chunking data")
        # 2048 ~43 seconds
        # 8192 ~172 seconds
        result_slices = slice_stoke(result, 8192)
        resultV_slices = slice_stoke(resultV, 8192)
        print("Data chunked, beginning predictions")
        for sliceI, sliceV in zip(result_slices, resultV_slices):
            output_predictions(sliceI, sliceV, obs_id, ratio)
        # pdb.set_trace()

    else:
        output_predictions(result, resultV, obs_id, ratio)
    

    # poly_time = time.time() - poly_start
    run_time = time.time() - run_start
    # print(f"Time to load data {load_time}s")
    # print(f"Time to predict spikes {predict_time}s")
    # print(f"Time to collect spike parameters {parse_time}s")
    # print(f"Time to generate TFCat {poly_time}s")
    print(f"Total run time {run_time}s")